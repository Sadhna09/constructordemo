﻿using System;

namespace Constructor
{
    public class Customer
    {

        public string firstName;
        public string lastName;

        public Customer() : this("no firstname", "no lastname")
        {

        }
        public Customer(string FirstName, string LastName)
        {
            this.firstName = FirstName;
            this.lastName = LastName;
        }
        public void PrintFullName()
        {
            Console.WriteLine("Fullname " + "" + this.firstName + this.lastName);
        }
    }
    public class Program
    {



        static void Main(string[] args)
        {
            Customer C1 = new Customer();
            C1.PrintFullName();
            Customer C2 = new Customer("Sadhna ", "Bharti");
            C2.PrintFullName();
        }
    }
}
